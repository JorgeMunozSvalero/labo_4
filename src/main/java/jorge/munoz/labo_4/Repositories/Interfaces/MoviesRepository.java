package jorge.munoz.labo_4.Repositories.Interfaces;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import jorge.munoz.labo_4.Repositories.Entities.MovieEntity;

public interface MoviesRepository
    extends JpaRepository<MovieEntity, Long>{

        @Query(value = "SELECT m FROM Movies m WHERE m.title LIKE :title%")
        Collection<MovieEntity> findMovieByTitle(@Param("title")String movieTitle);

        @Query(value = "SELECT m FROM Movies m WHERE m.year = :year")
        Collection<MovieEntity> findMovieByYear(@Param("year")int year);

        @Query(value = "SELECT m FROM Movies m WHERE ((m.title LIKE :title%) AND (m.year = :year))")
        Collection<MovieEntity> findMovieByTitleAndYear(@Param("title")String title, @Param("year")int year );

}
