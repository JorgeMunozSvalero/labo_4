package jorge.munoz.labo_4.Services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import jorge.munoz.labo_4.Repositories.Entities.MovieEntity;
import jorge.munoz.labo_4.Repositories.Interfaces.MoviesRepository;
import jorge.munoz.labo_4.Services.Models.MovieDTO;

public class MoviesService {
    @Autowired
    private MoviesRepository moviesRepository;
    @Autowired
    private ModelMapper modelMappper;

    public List<MovieDTO> getAll(){
        return moviesRepository.findAll().stream()
        .map(x -> modelMappper.map(x,MovieDTO.class))
        .collect(Collectors.toList());
    }

    public MovieDTO getById(Long id) {
        Optional<MovieDTO> lista = moviesRepository.findById(id)
        .map(x->modelMappper.map(x, MovieDTO.class));
        if(lista.isPresent()){
            return lista.get();
        }
        return null;
}

    public List<MovieDTO> getByTitle(String title){
            return moviesRepository.findMovieByTitle(title).stream()
            .map(x->modelMappper.map(x, MovieDTO.class))
            .collect(Collectors.toList());
    }

    public List<MovieDTO> getByYear(int year){
            return moviesRepository.findMovieByYear(year).stream()
            .map(x->modelMappper.map(x, MovieDTO.class))
            .collect(Collectors.toList());
    }

    public List<MovieDTO> getByTitleAndYear(String title, Integer year){
            if(title!=null && year == null){
                return getByTitle(title);
            }
            if(title==null && year != null){
                return getByYear(year);
            }
            if(title != null && year != null){
                return moviesRepository.findMovieByTitleAndYear(title, year).stream()
                .map(x->modelMappper.map(x, MovieDTO.class))
                .collect(Collectors.toList());
            }
            return getAll();
            
    }

    public MovieDTO add(MovieDTO user){
        MovieEntity entityToInsert = modelMappper.map(user, MovieEntity.class);
        MovieEntity result = moviesRepository.save(entityToInsert);
        return modelMappper.map(result, MovieDTO.class);
    }

    public Optional<MovieDTO> update(Long ID, MovieDTO movie){
        Optional<MovieEntity> dataToUpdate = moviesRepository.findById(ID);
        if(dataToUpdate.isPresent()){
            if(dataToUpdate.get().getId() == ID){
                MovieEntity entityToUpdate = modelMappper.map(movie, MovieEntity.class);
                entityToUpdate.setId(ID);
                MovieEntity result = moviesRepository.save(entityToUpdate);
                return Optional.of(modelMappper.map(result, MovieDTO.class));
            }
        }
        return Optional.empty();
    }

    public void delete(Long ID){
        Optional<MovieEntity> entityToDelete = moviesRepository.findById(ID);
        if(entityToDelete.isPresent()){
            moviesRepository.delete(entityToDelete.get());
        }
    }

    public MovieDTO findbyId(Long id){
        Optional<MovieEntity> entity = moviesRepository.findById(id);
        if(entity.isPresent()){
            return modelMappper.map(entity.get(), MovieDTO.class);
        }else{
            return null;
        }
    }

}
