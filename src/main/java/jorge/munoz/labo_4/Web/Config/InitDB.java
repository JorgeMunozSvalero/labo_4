package jorge.munoz.labo_4.Web.Config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jorge.munoz.labo_4.Repositories.Entities.MovieEntity;
import jorge.munoz.labo_4.Repositories.Interfaces.MoviesRepository;

@Configuration
public class InitDB {
    @Bean
    CommandLineRunner initDatabase(MoviesRepository moviesRepository) {
        return args -> {
            for(int i = 1; i<=10; i++){   
                moviesRepository.save(new MovieEntity("100"+i, (1990+i), "https://picsum.photos/200/300", "Info", "director"));
            }
        };
    }
}