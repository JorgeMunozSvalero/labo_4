package jorge.munoz.labo_4.Web.Config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jorge.munoz.labo_4.Services.MoviesService;

@Configuration
public class DI {
    @Bean
    MoviesService createUsersService(){
        return new MoviesService();
    }

    @Bean
    ModelMapper createModelMapper(){
        return new ModelMapper();
    }
}
