package jorge.munoz.labo_4.Web.API.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jorge.munoz.labo_4.Services.MoviesService;
import jorge.munoz.labo_4.Services.Models.MovieDTO;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("movies/v1")
public class MoviesController {
    private final MoviesService moviesService;

    MoviesController(MoviesService moviesService){
        this.moviesService = moviesService;
    }

    @GetMapping("/list")
    public List<MovieDTO> GetMovies(@RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "year", required = false) Integer year) {

                if((title == null) && (year == null)){
                    return moviesService.getAll();
                }

                if ((title != null) && (year != null)) {
                    
                    return moviesService.getByTitleAndYear(title, year);
                }
                if ((title != null) || (year != null)) {
                    if(title != null){
                        return moviesService.getByTitle(title);
                    }else{
                        return moviesService.getByYear(year);
                    }
                }

        return moviesService.getAll();

    }

    @GetMapping("/{id}")
    public MovieDTO getMovieDetailbyID(@PathVariable("id") Long id) {
        return moviesService.findbyId(id);
    }

    /*
    CRUD METHODS
    */
    @PostMapping("/movie")
    public MovieDTO AddMovie(@RequestBody MovieDTO movie) {
        return moviesService.add(movie);
    }

    @PutMapping("/update/{id}")
    public Optional<MovieDTO> UpdateMovie(@RequestBody MovieDTO movie, @PathVariable("id") Long id) {
        return moviesService.update(id, movie);
    }

    @DeleteMapping("/delete/{id}")
    public void DeleteMovie (@PathVariable("id") Long id) {
        moviesService.delete(id);
    }

}
