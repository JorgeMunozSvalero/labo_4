package jorge.munoz.labo_4.Web.View;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jorge.munoz.labo_4.Services.MoviesService;

@Controller
@RequestMapping("movies")
public class MoviesViewController {
    private final MoviesService moviesService;

    MoviesViewController(MoviesService moviesService){
        this.moviesService = moviesService;
    }

    @GetMapping()
    public ModelAndView Movies() {
        ModelAndView mv = new ModelAndView("searchTH");
        mv.addObject("movie", moviesService.getAll());
        return mv;
    }

    @GetMapping("/search")
    public ModelAndView listMovies(@RequestParam(name = "title", required = false) String title,
    @RequestParam(name = "year", required = false) Integer year){
        ModelAndView mv = new ModelAndView("searchTH");
        mv.addObject("movie", moviesService.getByTitleAndYear(title, year));
        return mv;
    }

    @GetMapping("/detail/{id}")
    public ModelAndView loadDetail(@PathVariable("id") Long id){
        ModelAndView mv = new ModelAndView("detailMovie");
        mv.addObject("movie", moviesService.getById(id));
        return mv;
    }



}
